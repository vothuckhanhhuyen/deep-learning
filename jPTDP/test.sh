python jPTDP/jPTDP.py --predict \
        --model jPTDP/sample/trialmodel \
        --params jPTDP/sample/trialmodel.params \
        --test BKTreebank1.0/test.conllu \
        --outdir BKTreebank1.0/ \
        --output test.conllu.pred
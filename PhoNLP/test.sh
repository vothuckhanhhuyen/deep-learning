python3 PhoNLP/phonlp/models/run_phonlp.py \
    --mode eval \
    --save_dir PhoNLP/phonlp/models/phonlp_tmp \
	--batch_size 8 \
	--eval_file_pos BKTreebank1.0/test_pos_all.txt \
	--eval_file_ner BKTreebank1.0/test_ner_all.txt \
	--eval_file_dep BKTreebank1.0/test.conllu 
python3 PhoNLP/phonlp/models/run_phonlp.py \
    --mode train \
    --save_dir PhoNLP/phonlp/models/phonlp_tmp \
	--pretrained_lm "vinai/phobert-base" \
	--lr 1e-5 --batch_size 32 --num_epoch 40 \
	--lambda_pos 0.5 --lambda_ner 0 --lambda_dep 0.5 \
	--train_file_pos BKTreebank1.0/train_pos_all.txt --eval_file_pos BKTreebank1.0/dev_pos_all.txt \
	--train_file_ner  PhoNLP/phonlp/sample_data/ner_train.txt --eval_file_ner  PhoNLP/phonlp/sample_data/ner_valid.txt \
	--train_file_dep BKTreebank1.0/train.conllu --eval_file_dep BKTreebank1.0/dev.conllu \
	--cpu
# deep-learning

## To run

### BiLSTM (Transition-based)
```
bash bist-parser/barchybrid/train.sh
bash bist-parser/barchybrid/test.sh
```

### BiLSTM (Graph-based)
```
bash bist-parser/bmstparser/train.sh
bash bist-parser/bmstparser/test.sh
```

### jPTDP
```
bash jPTDP/train.sh
bash jPTDP/test.sh
```

### PhoNLP
```
bash PhoNLP/train.sh
bash PhoNLP/test.sh
```

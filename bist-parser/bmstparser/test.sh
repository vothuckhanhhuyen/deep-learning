python bist-parser/bmstparser/src/parser.py \
    --predict \
    --outdir BKTreebank1.0 \
    --test BKTreebank1.0/test.pospred.conllu \
    --model bist-parser/bmstparser/pospred_notemb/neuralfirstorder.model30 \
    --params bist-parser/bmstparser/pospred_notemb/params.pickle
# python bist-parser/bmstparser/src/parser.py \
#     --predict \
#     --outdir BKTreebank1.0 \
#     --test BKTreebank1.0/test.pospred.conllu \
#     --model bist-parser/bmstparser/pospred_emb/neuralfirstorder.model30 \
#     --params bist-parser/bmstparser/pospred_emb/params.pickle
# python bist-parser/bmstparser/src/parser.py \
#         --dynet-seed 123456789 \
#         --dynet-mem 9999 \
#         --outdir bist-parser/bmstparser/pospred_notemb \
#         --train BKTreebank1.0/train.pospred.conllu \
#         --dev BKTreebank1.0/dev.pospred.conllu \
#         --epochs 30 \
#         --lstmdims 125 \
#         --lstmlayers 2 \
#         --bibi-lstm \
#         --pembedding 0
python bist-parser/bmstparser/src/parser.py \
        --dynet-seed 123456789 \
        --dynet-mem 9999 \
        --outdir bist-parser/bmstparser/pospred_emb \
        --train BKTreebank1.0/train.pospred.conllu \
        --dev BKTreebank1.0/dev.pospred.conllu \
        --epochs 30 \
        --lstmdims 125 \
        --lstmlayers 2 \
        --bibi-lstm
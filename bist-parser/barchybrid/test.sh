# python bist-parser/barchybrid/src/parser.py \
#     --predict \
#     --outdir BKTreebank1.0 \
#     --test BKTreebank1.0/test.pospred.conllu \
#     --model bist-parser/barchybrid/pospred_notemb/barchybrid.model30 \
#     --params bist-parser/barchybrid/pospred_notemb/params.pickle
python bist-parser/barchybrid/src/parser.py \
    --predict \
    --outdir BKTreebank1.0 \
    --test BKTreebank1.0/test.pospred.conllu \
    --model bist-parser/barchybrid/pospred_emb/barchybrid.model30 \
    --params bist-parser/barchybrid/pospred_emb/params.pickle